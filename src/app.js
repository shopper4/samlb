var teamListStyle = {
    display: 'flex',
    flexFlow: 'row wrap'
};

var teamStyle = {
    border: '1px solid lightGrey',
    padding: '5px',
    flexGrow: '1',
    width: '200px',
    margin: 'auto',
    fontFamily: 'sans-serif'
};

var favoriteTeamStyle = {
    backgroundColor: 'lightBlue',
    border: '1px solid lightGrey',
    padding: '5px',
    flexGrow: '1',
    width: '200px',
    margin: 'auto',
    fontFamily: 'sans-serif'
};

var TeamsList = React.createClass({

    loadTeams: function () {
        $.ajax({
            url: '/teams',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({ teams: data });
            }.bind(this),
            error: function (xhr, status, err) {
                console.error('/teams', status, err.toString());
            }.bind(this)
        });
    },

    handleFavoriteChanged: function (code) {
        if (this.state.favoriteTeams.indexOf(code) > -1) {
            this.state.favoriteTeams.splice(this.state.favoriteTeams.indexOf(code), 1);
        } else {
            this.state.favoriteTeams.push(code);
        }

        console.log(this.state.favoriteTeams);

        this.setState({
            favoriteTeams: this.state.favoriteTeams
        });
    },

    handleAddAll: function () {
        this.state.teams.forEach(team => {
            if (this.state.favoriteTeams.indexOf(team.code) === -1) {
                this.state.favoriteTeams.push(team.code);
            }
        });

        this.setState({
            favoriteTeams: this.state.favoriteTeams
        });
    },

    handleRemoveAll: function () {
        this.setState({
            favoriteTeams: []
        });
    },

    getInitialState: function () {
        return { teams: [], favoriteTeams: ['nyn'] };
    },

    componentDidMount: function () {
        this.loadTeams();
    },

    render: function () {
        var teamInfoList = this.state.teams.map(team => {
            var isFavorite = this.state.favoriteTeams.indexOf(team.code) > -1;
            return (
                <TeamInfo
                    team={team}
                    isFavorite={isFavorite}
                    onFavoriteChanged={this.handleFavoriteChanged}
                    key={team.code} />
            );
        });

        return (
            <div style={teamListStyle}>
                {teamInfoList}
                <AddAllButton addAllClick={this.handleAddAll} />
                <RemoveAllButton removeAllClick={this.handleRemoveAll} />
            </div>
        );
    }
})

var TeamInfo = React.createClass({
    handleClick: function (event) {
        this.props.onFavoriteChanged(this.props.team.code);
    },

    render: function () {
        var style = this.props.isFavorite ? favoriteTeamStyle : teamStyle;
        
        return (
            <div onClick={this.handleClick} style={style}>
                {this.props.team.name}
            </div>
        );
    }
});

var AddAllButton = React.createClass({
    handleClick: function (event) {
        this.props.addAllClick();
    },

    render: function () {
        return (
            <button onClick={this.handleClick}>Add All</button>
        );
    }
});

var RemoveAllButton = React.createClass({
    handleClick: function (event) {
        this.props.removeAllClick();
    },

    render: function () {
        return (
            <button onClick={this.handleClick}>Remove All</button>
        );
    }
});

function run() {
    React.render(<TeamsList />, document.getElementById('example'));
}

if (window.addEventListener) {
    window.addEventListener('DOMContentLoaded', run);
} else {
    window.attachEvent('onload', run);
}