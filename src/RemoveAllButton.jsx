var React = require('react');

module.exports = React.createClass({
    handleClick: function (event) {
        this.props.removeAllClick();
    },

    render: function () {
        return (
            <button onClick={this.handleClick}>Remove All</button>
        );
    }
});