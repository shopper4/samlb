var React = require('react');

module.exports = React.createClass({
    handleClick: function (event) {
        this.props.addAllClick();
    },

    render: function () {
        return (
            <button onClick={this.handleClick}>Add All</button>
        );
    }
});