var React = require('react');

var teamStyle = {
    border: '1px solid lightGrey',
    padding: '5px',
    flexGrow: '1',
    width: '200px',
    margin: 'auto',
    fontFamily: 'sans-serif'
};

var favoriteTeamStyle = {
    backgroundColor: 'lightBlue',
    border: '1px solid lightGrey',
    padding: '5px',
    flexGrow: '1',
    width: '200px',
    margin: 'auto',
    fontFamily: 'sans-serif'
};

module.exports = React.createClass({
    handleClick: function (event) {
        this.props.onFavoriteChanged(this.props.team.code);
    },

    render: function () {
        var teamInfo = this.props.isFavorite ? (
                            <div onClick={this.handleClick} style={favoriteTeamStyle}>
                                {this.props.team.name}
                            </div>
                        ) : (
                            <div onClick={this.handleClick} style={teamStyle}>
                                {this.props.team.name}
                            </div>
                        );

        return teamInfo;
    }
});