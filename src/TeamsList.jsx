var React = require('react');
var TeamInfo = require('./TeamInfo.jsx');
var AddAllButton = require('./AddAllButton.jsx');
var RemoveAllButton = require('./RemoveAllButton.jsx');

module.exports = React.createClass({

    loadTeams: function () {
        $.ajax({
            url: '/teams',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({ teams: data });
            }.bind(this),
            error: function (xhr, status, err) {
                console.error('/teams', status, err.toString());
            }.bind(this)
        });
    },

    handleFavoriteChanged: function (code) {
        if (this.state.favoriteTeams.indexOf(code) > -1) {
            this.state.favoriteTeams.splice(this.state.favoriteTeams.indexOf(code), 1);
        } else {
            this.state.favoriteTeams.push(code);
        }

        console.log(this.state.favoriteTeams);

        this.setState({
            favoriteTeams: this.state.favoriteTeams
        });
    },

    handleAddAll: function () {
        this.state.teams.forEach(team => {
            if (this.state.favoriteTeams.indexOf(team.code) === -1) {
                this.state.favoriteTeams.push(team.code);
            }
        });

        this.setState({
            favoriteTeams: this.state.favoriteTeams
        });
    },

    handleRemoveAll: function () {
        this.setState({
            favoriteTeams: []
        });
    },

    getInitialState: function () {
        return { teams: [], favoriteTeams: ['nyn'] };
    },

    componentDidMount: function () {
        this.loadTeams();
    },

    render: function () {
        var teamInfoList = this.state.teams.map(team => {
            var isFavorite = this.state.favoriteTeams.indexOf(team.code) > -1;
            return (
                <TeamInfo
                    team={team}
                    isFavorite={isFavorite}
                    onFavoriteChanged={this.handleFavoriteChanged}
                    key={team.code} />
            );
        });

        return (
            <div style={teamListStyle}>
                {teamInfoList}
                <AddAllButton addAllClick={this.handleAddAll} />
                <RemoveAllButton removeAllClick={this.handleRemoveAll} />
            </div>
        );
    }
});