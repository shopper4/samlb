var request = require('request');
var moment = require('moment-timezone');

var samlb = {

    generateURL: function () {
        var year = moment().year();
        var month = moment().month() + 1;
        var day = moment().date();

        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;

        return 'http://mlb.mlb.com/gdcross/components/game/mlb/year_YYYY/month_MM/day_DD/grid.json'.replace('YYYY', year).replace('MM', month).replace('DD', day);
    },

    getTeams: function (url, callback) {
        var ret = [];

        request({
            url: url || this.generateURL(),
            json: true
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                if (body.data && body.data.games && body.data.games.game) {
                    body.data.games.game.forEach(function (g) {
                        ret.push({ name: g.home_team_name, code: g.home_code });
                        ret.push({ name: g.away_team_name, code: g.away_code });
                    });
                }
                if (callback) callback(null, ret);
            } else {
                console.log(error);
            }
        });
    },

    getTeamsStatic: function (err, callback) {
        callback(null, samlb.utils.sortTeams(samlb.staticData.teams));
    },

    getGames: function (url, callback) {
        var ret = [];
        request({
            url: url || this.generateURL(),
            json: true
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                if (body.data && body.data.games && body.data.games.game) {
                    ret = body.data.games.game;
                }
                if (callback) callback(ret);
            }
        });
    },

    getTodaysGames: function (callback) {
        request({
            url: this.generateURL(),
            json: true
        }, function (err, response, body) {
            if (!err && response.statusCode === 200) {
                callback(null, body.data.games.game);
            } else {
                callback(err, null);
            }
        });
    },

    generateSubscriptions: function (userPrefs, games, callback) {
        var subsToNotify = [];

        userPrefs.forEach(function (pref) {
            var matchingGames = games.filter(function (game) {
                if (pref.teams.indexOf(game.home_code) > -1 || pref.teams.indexOf(game.away_code) > -1) {
                    
                }
            });
        });
    },

    db: {

    },

    utils: {
        parseTime: function (time) {
            var _time = time;
            var _hour = 0,
                _minute = 0;
            
            if (_time.indexOf(' PM') > -1) {
                _time = _time.substring(0, _time.indexOf(' PM'));
                _hour = Number(_time.split(':')[0]) + 12;
            } else if (_time.indexOf(' AM') > -1) {
                _time = _time.substring(0, _time.indexOf(' AM'));
                _hour = Number(_time.split(':')[0]);
            }
            
            _minute = Number(_time.split(':')[1]);
            
            return { hour: _hour, minute: _minute };
        },

        getGameTime: function (time) {
            if (typeof time === 'string') time = samlb.utils.parseTime(time);

            return moment.tz(moment().format(), 'America/New_York')
                            .hour(time.hour)
                            .minute(time.minute);
        },

        // I'd like to do a more generic function to convert any timezone,
        // but do a quick UK one to get it working for me
        getUKTime: function (time) {

            if (typeof time === 'string') time = samlb.utils.parseTime(time);

            var now = moment.tz(moment().format(), 'America/New_York').hour(time.hour);
            now.minute(time.minute);

            return now.clone().tz("Europe/London");
        },

        parseUKTime: function (time) {

            if (typeof time === 'string') time = samlb.utils.parseTime(time);

            var now = moment.tz(moment().format(), 'Europe/London').hour(time.hour);
            now.minute(time.minute);

            return now.clone().tz("Europe/London");
        },

        sortTeams: function (teams) {
            return teams.sort(function (a, b) {
                // if (a.name > b.name) {
                //     return 1;
                // }

                // if (a.name < b.name) {
                //     return -1;
                // }

                // return 0;

                return a.name === b.name ? 0 : (a.name > b.name ? 1 : -1);
            });
        }
    },

    staticData: {
        teams: [
            {"name":"Mets","code":"nyn"},
            {"name":"D-backs","code":"ari"},
            {"name":"Marlins","code":"mia"},
            {"name":"Reds","code":"cin"},
            {"name":"Rays","code":"tba"},
            {"name":"Astros","code":"hou"},
            {"name":"Indians","code":"cle"},
            {"name":"Athletics","code":"oak"},
            {"name":"Red Sox","code":"bos"},
            {"name":"Yankees","code":"nya"},
            {"name":"Orioles","code":"bal"},
            {"name":"Nationals","code":"was"},
            {"name":"Twins","code":"min"},
            {"name":"Tigers","code":"det"},
            {"name":"Royals","code":"kca"},
            {"name":"Blue Jays","code":"tor"},
            {"name":"Cubs","code":"chn"},
            {"name":"White Sox","code":"cha"},
            {"name":"Rangers","code":"tex"},
            {"name":"Padres","code":"sdn"},
            {"name":"Giants","code":"sfn"},
            {"name":"Phillies","code":"phi"},
            {"name":"Rockies","code":"col"},
            {"name":"Braves","code":"atl"},
            {"name":"Mariners","code":"sea"},
            {"name":"Angels","code":"ana"},
            {"name":"Dodgers","code":"lan"},
            {"name":"Brewers","code":"mil"},
            {"name":"Pirates","code":"pit"},
            {"name":"Cardinals","code":"sln"}
        ]
    }
};

module.exports = samlb;
