var express = require('express');
var app = express();
var samlb = require('./samlb');

app.use(express.static(__dirname));

app.get('/', function (request, response) {
    response.sendFile(__dirname + '/index.html');
});

/** API **/
app.get('/games', function (request, response) {
    try {
        console.log('Getting todays games...');
        samlb.getTodaysGames(function (err, games) {
            if (!err && games) {
                response.json(games);
            } else {
                console.log(err);
            }
        });    
    } catch (ex) {
        console.log(ex);
        response.send(404);
    }
});

app.get('/games/:teamCode', function (request, response) {
    var teamCode = request.params.teamCode;

    try {
        samlb.getTodaysGames(function (err, games) {
            if (!err && games) {
                response.json(games.filter(function (game) {
                    return game.home_code === teamCode || game.away_code === teamCode;
                }));
            } else {
                console.log(err);
            }
        });    
    } catch (ex) {
        console.log(ex);
        response.send(404);
    } 
});

app.get('/games/time/:time', function (request, response) {
    var maxTime = request.params.time;

    try {
        samlb.getTodaysGames(function (err, games) {
            if (!err && games) {
                response.json(games.filter(function (game) {
                    var gameTime = samlb.utils.getGameTime(game.event_time);
                    var ukTime = samlb.utils.getUKTime(game.event_time);

                    var maxGameTime = samlb.utils.parseUKTime(maxTime);

                    console.log('gameTime:', gameTime.format());
                    console.log('ukTime:', ukTime.format());
                    console.log('maxGameTime:', maxGameTime.format());

                    return gameTime <= maxGameTime;
                }));
            } else {
                console.log(err);
            }
        })
    } catch (ex) {
        console.log(ex);
        response.send(404);
    }
});

app.get('/teams', function (request, response) {
    try {
        // samlb.getTeams(null, function (err, teams) {
        //     if (!err && teams) {
        //         response.json(teams);
        //     } else {
        //         console.log(err);
        //     }
        // });

        samlb.getTeamsStatic(null, function (err, teams) {
            if (!err && teams) {
                response.json(teams);
            } else {
                console.log(err);
            }
        });
    } catch (ex) {
        console.log(ex);
    }
});

app.listen(process.env.PORT || 3333);
/** API End **/