'use strict';

var teamListStyle = {
    display: 'flex',
    flexFlow: 'row wrap'
};

var teamStyle = {
    border: '1px solid lightGrey',
    padding: '5px',
    flexGrow: '1',
    width: '200px',
    margin: 'auto',
    fontFamily: 'sans-serif'
};

var favoriteTeamStyle = {
    backgroundColor: 'lightBlue',
    border: '1px solid lightGrey',
    padding: '5px',
    flexGrow: '1',
    width: '200px',
    margin: 'auto',
    fontFamily: 'sans-serif'
};

var TeamsList = React.createClass({
    displayName: 'TeamsList',

    loadTeams: function loadTeams() {
        $.ajax({
            url: '/teams',
            dataType: 'json',
            cache: false,
            success: (function (data) {
                this.setState({ teams: data });
            }).bind(this),
            error: (function (xhr, status, err) {
                console.error('/teams', status, err.toString());
            }).bind(this)
        });
    },

    handleFavoriteChanged: function handleFavoriteChanged(code) {
        if (this.state.favoriteTeams.indexOf(code) > -1) {
            this.state.favoriteTeams.splice(this.state.favoriteTeams.indexOf(code), 1);
        } else {
            this.state.favoriteTeams.push(code);
        }

        console.log(this.state.favoriteTeams);

        this.setState({
            favoriteTeams: this.state.favoriteTeams
        });
    },

    handleAddAll: function handleAddAll() {
        var _this = this;

        this.state.teams.forEach(function (team) {
            if (_this.state.favoriteTeams.indexOf(team.code) === -1) {
                _this.state.favoriteTeams.push(team.code);
            }
        });

        this.setState({
            favoriteTeams: this.state.favoriteTeams
        });
    },

    handleRemoveAll: function handleRemoveAll() {
        this.setState({
            favoriteTeams: []
        });
    },

    getInitialState: function getInitialState() {
        return { teams: [], favoriteTeams: ['nyn'] };
    },

    componentDidMount: function componentDidMount() {
        this.loadTeams();
    },

    render: function render() {
        var _this2 = this;

        var teamInfoList = this.state.teams.map(function (team) {
            var isFavorite = _this2.state.favoriteTeams.indexOf(team.code) > -1;
            return React.createElement(TeamInfo, {
                team: team,
                isFavorite: isFavorite,
                onFavoriteChanged: _this2.handleFavoriteChanged,
                key: team.code });
        });

        return React.createElement(
            'div',
            { style: teamListStyle },
            teamInfoList,
            React.createElement(AddAllButton, { addAllClick: this.handleAddAll }),
            React.createElement(RemoveAllButton, { removeAllClick: this.handleRemoveAll })
        );
    }
});

var TeamInfo = React.createClass({
    displayName: 'TeamInfo',

    handleClick: function handleClick(event) {
        this.props.onFavoriteChanged(this.props.team.code);
    },

    render: function render() {
        var teamInfo = this.props.isFavorite ? React.createElement(
            'div',
            { onClick: this.handleClick, style: favoriteTeamStyle },
            this.props.team.name
        ) : React.createElement(
            'div',
            { onClick: this.handleClick, style: teamStyle },
            this.props.team.name
        );

        return teamInfo;
    }
});

var AddAllButton = React.createClass({
    displayName: 'AddAllButton',

    handleClick: function handleClick(event) {
        this.props.addAllClick();
    },

    render: function render() {
        return React.createElement(
            'button',
            { onClick: this.handleClick },
            'Add All'
        );
    }
});

var RemoveAllButton = React.createClass({
    displayName: 'RemoveAllButton',

    handleClick: function handleClick(event) {
        this.props.removeAllClick();
    },

    render: function render() {
        return React.createElement(
            'button',
            { onClick: this.handleClick },
            'Remove All'
        );
    }
});

function run() {
    React.render(React.createElement(TeamsList, null), document.getElementById('example'));
}

if (window.addEventListener) {
    window.addEventListener('DOMContentLoaded', run);
} else {
    window.attachEvent('onload', run);
}