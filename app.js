var express = require('express');
var app = express();
var schedule = require('node-schedule');
var samlb = require('./samlb');
var moment = require('moment-timezone');

/** API **/
app.get('/games', function (request, response) {
    response.json(samlb.getTodaysGames);
});
/** API End **/

//moment.tz.setDefault("America/New_York")

var todaysGames = {
    games: [],
    lastUpdated: null
};

var schedules = {
    gameUpdate: '07 * * * *',
    notifySubscribers: '08 * * * *'
};

var userPrefs = [];
userPrefs.push({
    name: 'Sam',
    email: 'shopper4@gmail.com',
    teams: ['sfg'],
    time: '21:00'
});
// userPrefs.push({
//     name: 'Jack',
//     email: 'dancon92@gmail.com',
//     teams: ['sfg'],
//     time: '9:00PM'
// });

// Gets the list of games from mlb.com once per day
var gameUpdate = schedule.scheduleJob(schedules.gameUpdate, function() {
    console.log('Updating game data...');

    todaysGames.games = samlb.getGames();

    if (todaysGames.games && todaysGames.games.length > 0) {
        todaysGames.lastUpdated = moment();
    } else {
        todaysGames.games = [];
    }
});

var notifySubscribers = schedule.scheduleJob(schedules.notifySubscribers, function() {
    console.log('Notifying subscribers...');
    todaysGames.games.forEach(function(g) {
        console.log(g.away_team_name + ' @ ' + g.home_team_name + ' - ' + g.event_time + ' ET');
    });
});


samlb.getGames(null, function(games) {
    if (games) {
        userPrefs.forEach(function(pref) {
            games.forEach(function(g) {

                //var maxTime = convertTimezone(parseTime(pref.time), 'Europe/London', 'America/New_York');
                var maxTime = parseTime(pref.time);
                //var gameTime = convertTimezone(parseTime(g.event_time), 'America/New_York', 'Europe/London');
                var gameTime = parseTime(g.event_time, 'America/New_York');

                //  console.log('maxTime: ' + maxTime.format());
                //  console.log('gameTime: ' + gameTime.tz('Europe/London').format());
                //console.log(gameTime.isBefore(maxTime));

                if (gameTime.isBefore(maxTime)) {
                    console.log(g.away_team_name + ' @ ' + g.home_team_name + ' - ' + g.event_time + ' ET');
                    console.log(gameTime.format());
                }
            });
        });
    }
});

// var timeString = '1:05 PM';
// var hour = timeString.split(':')[0];
// var minute = timeString.split(':')[1].split(' ')[0];
// hour = hour + 13;

// var currentTime = moment().hour(hour).minute(minute);

function parseTime(timeString, timeZone) {
    if (timeString === '') return null;

    var time = timeString.match(/(\d+)(:(\d\d))?\s*(p?)/i);
    if (time === null || typeof time === 'undefined') return null;

    var hours = parseInt(time[1], 10);
    if (hours == 12 && !time[4]) {
        hours = 0;
    } else {
        hours += (hours < 12 && time[4]) ? 12 : 0;
    }

    var d = new moment();
    d.hours(hours);
    d.minutes(parseInt(time[3], 10) || 0);
    d.seconds(0);

    if (timeZone) {
        d = d.clone().tz(timeZone);
    }

    return d;
}

function convertTimezone(date, fromZone, toZone) {

    if (!fromZone) fromZone = 'America/New_York';
    if (!toZone) toZone = 'Europe/London';

    //var fromDate = date.clone().tz(fromZone);
    return date.clone().tz(toZone);
}